<p>DevChallenge 12 JS (pro) - Round2</p>

Проект виконаний з використанням фреймворку Angular 6.0

- Зібраний проект знаходиться в папці "dist/" - достатньо скопіювати файли в середині данної папки на статичний сервер
- Вихідні файли знаходяться в папці "src/"
- Основний код знаходиться в файлах та папках: 

        "src/app.component.html"
        "src/app.component.scss"
        "src/app.component.ts" - файли загального компоненту - відображає вікно роутінгу для інших сторінок та header.

        "src/admin-mode/" - папка компоненту, який відповідає за сторінку Адміністрування (/admin) (доступна після успішної авторизації - login\password : "admin")

        "src/user-mode" - папка компоненту, який відповідає за сторінку Користувача (/user) (єдина доступна без авторизації)

        "src/login-page" - папка компоненту, який відповідє за сторінку авторизації (/login)

        "src/dialog-remove/" - папка допоміжного компоненту, який відповідає за вікно запиту (Material) на видалення із БД/JSON (localStorage в даному випадку)

        "src/dialog-stepper/" - папка допоміжного компоненту, який відповідає за вікно покрокового додавання/редагування в базі данних

        "src/services/authentication.service.ts" - сервіс, який відповідає за авторизацію
        "src/services/default-data.service.ts - початкові дані

        "src/app.module.ts" - підключення модулів


- Щоб встановити проект: 
        npm install

- Щоб зібрати проект для Dev версії (я використовував npm версії 6.0.0):

                npm run start:hmr
    або
                npm start

- Щоб зібрати проект для Prod версії:

                npm run build:prod

- !!!Авторизація!!!
        - кнопки відповідальні за авторизацію (вхід/вихід) знаходяться в правому верхньому куті
        - Логін та Пароль: admin
        - після успішної авторизації доступна сторінка авторизації /admin
        - змінні які зберігаються в Local Storage для проходження автоматично авторизації: library_key, library_user

- Дані зберігаються в вигляді JSON в Local Storage змінна: onlineLibrarian
  Якщо необхідно скинути память до базових данних, необхідно видалити onlineLibrarian із localStorage або видалити всі змінні із localStorage (тоді необхідно буде авторизуватись ще раз) та перезавантажити сторінку

- Статус книжок (На руках / Повернута) залежить від Часу повернення - якщо Час Повернення минув, то автоматично ставиться статус Повернута (при завантажені сторінки admin та сторінки user для обраної книжки)

- Функції сторінки User (сторінка Чат боту):
        - пошук книжок
        - вибір книжки за ID
        - пошук бібліотек для обраної книжки
        - вибір найближчої бібліотеки за маркером на карті
        - вибір бібліотеки за ID
        - генерація QR коду (вказано ID екземпляру книжки та дата бронювання) та можливість скачати його
        - бронювання книжки на 5 хвилин
        - підказки для кожного кроку в спілкуванні з чат ботом

- Функції сторінки Admin (сторінка Адміністрування)
        - додавання/видалення/редагування списку Бібліотек
        - додавання/видалення/редагування списку Книжок
        - додавання/видалення/редагування списку Екземплярів книжок (крос таблиця, в якій вказується статус окремого екземпляру книжки)
        - імпорт/експорт в json файл - кнопка в правому верхньому куті

- Функція сторінки авторизації (Login)
        - перевірка логіну та паролю (зашифрований в MD5)
        - при невдалій авторизації внизу виводиться сповіщення про неправильний пароль

- Приклади пошуку які я використовував:
        - "javascript"
        - "стив"
        - "java"
- Якщо хочете повторити пошук: відповідно підказці зліва - введіть "спочатку"

P.S. Сподіваюсь мій застосунок буде інтуїтивно зрозумілим.